/*setStr
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author ADMIN
 */
public class UpdateDatabase1 {

    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee.db";
        //connectdatabase
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connectin to SQLite has been establish");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        //selected
        String sql = "UPDATE category SET cat_name=? WHERE cat_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "MyCoffee");
            stmt.setInt(2,1);
            
            int status = stmt.executeUpdate();
//            ResultSet key = stmt.getGeneratedKeys();
//            key.next();
//            System.out.println("" + key.getInt(1));

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //closedatabase
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
